%% @doc Eventgenerator module
-module(eventgenerator).
-export([init/0]).
%% @doc Initializes the module and enters a loop which can receive the following messages:
%%      tick : Rolls a dice and determines if a random worker should become sick with a random sick time. 
%%              Also determines if a random worker should die or if a new worker should be hired.
%%      {'EXIT', _PID, terminate} : Exits normally.
init() ->
    loop().

loop() ->
    receive
        tick ->
            Chance = random:uniform(),
            Type = lists:nth(random:uniform(3), [chef, doctor, farmer]),
            workers ! {nr_workers, self()},
            receive
                {nr_workers, Nr_workers} ->
                    ok
            end,
            Sick_chance = Nr_workers * 0.002,
            Life_death_chance = Nr_workers * 0.0002,
            if
                Chance =< Life_death_chance ->
                    Flip_a_coin = random:uniform(),
                    if
                        Flip_a_coin > 0.5 ->
                            %%io:fwrite("killed\n"),
                            workers ! {remove, Type};
                        true ->
                            %%io:fwrite("new worker\n"),
                            workers ! {add, Type}
                    end;
                Chance =< Sick_chance ->
                    %%io:fwrite("sick\n"),
                    Sicktime = random:uniform(30),
                    workers ! {sick, Type, Sicktime};
                true ->
                    ok
            end,
            loop();
        {'EXIT', _PID, terminate} ->
            exit(normal)
    end.
