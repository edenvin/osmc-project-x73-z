import java.io.IOException;

import com.ericsson.otp.erlang.*;

public class TalkToMe {
	public OtpNode node;
	public OtpMbox mbox;
		/**
		 * Constuctor.
		 */
	public TalkToMe() {
		OtpNode node = null;
		try {
			node = new OtpNode("java@localhost");
		} catch (IOException e) {
			e.printStackTrace();
		}
		mbox = node.createMbox("gui");
	}
	/**
	 * Recieves information from erlang and converts it into data-types to be used by Java.
	 * @return an integer array with information recieved from erlang.
	 */
	public Integer[] receive() {
		OtpErlangTuple msg = null;
		try {
			msg = (OtpErlangTuple) mbox.receive();
		} catch (OtpErlangExit e) {
			e.printStackTrace();
		} catch (OtpErlangDecodeException e) {
			e.printStackTrace();
		}
		OtpErlangAtom from = (OtpErlangAtom) msg.elementAt(0);
		Integer[] array = new Integer[5];
		if (from.atomValue().compareTo("kitchen") == 0) {
			array[0] = 0;
			try {
				for (int i = 1; i < 5; i++) {
					array[i] = ((OtpErlangLong) msg.elementAt(i)).intValue();
				}
			} catch (OtpErlangRangeException e) {
				e.printStackTrace();
			}
		} else if (from.atomValue().compareTo("hospital") == 0) {
			array[0] = 1;
			try {
				for (int i = 1; i < 5; i++) {
					array[i] = ((OtpErlangLong) msg.elementAt(i)).intValue();
				}
			} catch (OtpErlangRangeException e) {
				e.printStackTrace();
			}
		} else if (from.atomValue().compareTo("farm") == 0) {
			array[0] = 2;
			try {
				for (int i = 1; i < 3; i++) {
					array[i] = ((OtpErlangLong) msg.elementAt(i)).intValue();
				}
			} catch (OtpErlangRangeException e) {
				e.printStackTrace();
			}
			array[3] = 0;
			array[4] = 0;
		} else if (from.atomValue().compareTo("restingroom") == 0) {
			array[0] = 3;
			try {
				for (int i = 1; i < 4; i++) {
					array[i] = ((OtpErlangLong) msg.elementAt(i)).intValue();
				}
			} catch (OtpErlangRangeException e) {
				e.printStackTrace();
			}
			array[4] = array[1] + array[2] + array[3];
		} else if (from.atomValue().compareTo("slow") == 0) {
			array[0] = 97;
			for (int i = 1; i < 5; i++) {
				array[i] = 0;
			}
		} else if (from.atomValue().compareTo("fast") == 0) {
			array[0] = 98;
			for (int i = 1; i < 5; i++) {
				array[i] = 0;
			}
		} else if (from.atomValue().compareTo("step") == 0) {
			array[0] = 99;
			for (int i = 1; i < 5; i++) {
				array[i] = 0;
			}
		}
		/*for(int i = 0; i < 5; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();*/
		return array;
	}
}
