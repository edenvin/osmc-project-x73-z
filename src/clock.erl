%% @doc Clock module
-module(clock).
-export([init/0] ).
%% @doc Initializes the module and enters a loop which can receive the following messages:
%%      step : Sends a clock signal to all processes that can receive one.
%%      {play, Speed} : Spawns a new process which sends step to the clock module every given Time.
%%      pause : Terminates the automatic stepper if there is one.
%%      {'EXIT', _PID, terminate} : Exits normally.
-spec init() -> ok.

init() ->
    process_flag(trap_exit, true),
    receive_loop().

receive_loop() ->
    receive
        step ->
            {gui, 'java@localhost'} ! {step},
            tick(),
            receive_loop();
        {play, Speed} ->
            Player = whereis(player),
            if
                Player =:= undefined ->
                    register(player, spawn_link(fun()->player(Speed) end)),
                    receive_loop();
                true ->
                    io:fwrite("The simulation is already running.\n"),
                    receive_loop()
            end;
        pause ->
            Player = whereis(player),
            if
                Player =/= undefined ->
                    exit(Player, stop),
                    unregister(player),
                    receive_loop();
                true ->
                    io:fwrite("No simulation is running.\n"),
                    receive_loop()
            end;
        {'EXIT', _PID, terminate} ->
            exit(normal)
    end.

player(Speed) ->
    timer:sleep(Speed),
    clock ! step,
    player(Speed).

tick() ->
    workers ! {all_workers, self()},
    receive
        {all_workers, All_workers} ->
            _ = [spawn_link(fun()->tick(PIDs) end) || {_, PIDs} <- All_workers]
    end,
    tick([eventgenerator, kitchen, hospital, farm]).

tick([]) ->
    ok;

tick([PID | PIDs]) ->
    PID ! tick, 
    tick(PIDs).
