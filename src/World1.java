import java.net.URL;
import javax.swing.*;
import java.awt.*;


@SuppressWarnings("serial")
public class World1 extends JFrame{

	Rectangle hero = new Rectangle(500, 500, 25,25);
	int steps;
	Image available[];
	Image doctors[];
	Image farmers[];
	Image sickies[];
	Image chefs[];
	Image resting[];
	Image img2;
	Image img3;
	Image img4;
	Image img5;
	boolean slow = true;

	TalkToMe ttm = new TalkToMe();

	Integer[] restingworkers = new Integer[5];
	Integer[] kitchen = new Integer[5];
	Integer[] farm = new Integer[3];
	Integer[] healthcare = new Integer[5];
	/**
	 * Run our program.
	 * @param args standard parameter
	 */
	public static void main(String [] args){
		new World1();
	}
	/**
	 * Instanciates an array for each room filled with n sprites.
	 */
	public void addSprites() {

		resting = new Image[(healthcare[4] + farm[2] + kitchen [4])];
		doctors = new Image[healthcare[3]];
		sickies = new Image[healthcare[2]];
		farmers = new Image[farm[1]];
		chefs = new Image[kitchen[3]];
		available = new Image[restingworkers[4]];

		for(int i = 0; i < (healthcare[4] + farm[2] + kitchen [4]); i++) {
			if(healthcare[4] > i )
				resting[i] = img2;
			else if(healthcare[4]+farm[2] > i)
				resting[i] = img3;
			else
				resting[i] = img4;
		}

		for(int i = 0; i < healthcare[2]; i++) {
			sickies[i] = img5;
		}


		for(int i = 0; i < (healthcare[3]); i++) {
			doctors[i] = img2;
		}
		for(int i = 0; i < farm[1]; i++) {
			farmers[i] = img3;
		}
		for(int i = 0; i < kitchen[3]; i++) {
			chefs[i] = img4;
		}

		for(int i = 0; i < restingworkers[4]; i++) {
			if(restingworkers[1] > i )
				available[i] = img2;
			else if(restingworkers[2]+restingworkers[1] > i)
				available[i] = img3;
			else
				available[i] = img4;
		}
	}
	/**
	 * Constructor for World1
	 */
	public World1(){
		for(int i= 0; i < kitchen.length; i++){
			kitchen[i] = 0;

		}
		for(int i= 0; i < farm.length; i++){
			farm[i] = 0;

		}
		for(int i= 0; i < healthcare.length; i++){
			healthcare[i] = 0;

		}
		for(int i= 0; i < restingworkers.length; i++){
			restingworkers[i] = 0;

		}

		URL url2 = World1.class.getResource("images/doctorSmall.png");
		URL url3 = World1.class.getResource("images/Farm-plot-icon.png");
		URL url4 = World1.class.getResource("images/Brave-chef-icon.png");
		URL url5 = World1.class.getResource("images/icon_bed.gif");

		img2 = new ImageIcon(url2).getImage();
		img3 = new ImageIcon(url3).getImage();
		img4 = new ImageIcon(url4).getImage();
		img5 = new ImageIcon(url5).getImage();
		setSize(1200,1000);
		setTitle(" X73-z ");	
		setDefaultCloseOperation(3);
		setVisible(true);
		setResizable(false);


	}
	/**
	 * Function used by swing to paint the stuff that needs to be repainted often.
	 */

	public void paint(Graphics g){

		Image offscreen = createImage(getWidth(), getHeight());
		talkToErl();
		draw(offscreen.getGraphics());
		g.drawImage(offscreen, 0, 0, null);
	}

	/**
	 * Receives information from erlang that is going to be printed/updated onto the GUI.
	 */
	public void talkToErl() {
		Integer[] temp = new Integer[5];  	
		boolean loop = slow;
		do {
			temp = ttm.receive();
			switch(temp[0]) {
			case 0:
				kitchen = temp;
				break;
			case 1:
				healthcare = temp;
				break;
			case 2:
				farm = temp;
				break;
			case 3:
				restingworkers = temp;
				break;
			case 97:
				slow = true;
				break;
			case 98:
				slow = false;
				break;
			default:
				loop = false;
			}
			} while(loop);
		steps++;
		addSprites();
	}



	/**
	 * Function used to draw everything.
	 * @param g Where everything in "draw" is to be painted
	 */
	public void draw(Graphics g){
		//Bakgrund
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, 1200, 1000);

		//Rum
		g.setColor(Color.BLUE); 	//topleft
		g.fillRect(200, 100, 300, 300);

		g.setColor(Color.RED);		//topright
		g.fillRect(600, 100, 300, 300);

		g.setColor(Color.GREEN);	//bottomleft
		g.fillRect(200, 500, 300, 300);

		g.setColor(Color.YELLOW);	//bottomright
		g.fillRect(600, 500, 300, 300);
		
		g.setColor(Color.BLUE);		//border
		g.drawLine(600, 250, 899, 250);

		//Workers! som ritas ut i boxarna!	
		int xd = 200;
		int yd = 100;
		//The kitchen
		if(kitchen[3] > 0){
			for (int i = 0; i < kitchen[3]; i++) {
				if(chefs.length > 100){
					g.setColor(Color.BLACK);		
					g.drawString(": " + chefs.length, 400, 400);
					g.drawImage(chefs[99], xd, yd, null); 
					xd += 30;
				}
				else if(xd > 470) {
					yd += 30;
					xd = 200;

				}
				g.drawImage(chefs[i], xd, yd, null);
				xd += 30;
			}
		}
		xd = 600;
		yd = 100;
		//hospital
		if(healthcare[3] > 0){
			for(int i = 0; i < doctors.length; i++) {
				if(doctors.length > 50){
					g.setColor(Color.BLACK);		
					g.drawString(": " + doctors.length, 800, 235);
					g.drawImage(doctors[45], xd, yd, null); 
					xd += 30;
				}
				else if(xd > 870) {
					yd += 30;
					xd = 600;

				}
				g.drawImage(doctors[i], xd, yd, null);
				xd += 30;
			}
		}		
		 	xd = 600;
		   	yd = 250;
	 		//The sickbay
		if(healthcare[2] > 0){
			for(int i = 0; i < sickies.length; i++) {
				if(sickies.length > 50){
					g.setColor(Color.BLACK);		
					g.drawString(": " + sickies.length, 800, 400);
					g.drawImage(sickies[45], xd, yd, null); 
					xd += 30;
				}
				else if(xd > 870) {
					yd += 30;
					xd = 600;

				}
				g.drawImage(sickies[i], xd, yd, null);
				xd += 30;
			}
		}

		xd = 200;
		yd = 500;
		// The farm
		if(farm[1] > 0){
			for(int i = 0; i < farmers.length; i++) {
				if(farmers.length > 100){
					g.setColor(Color.BLACK);		
					g.drawString(": " + farmers.length, 400, 800);
					g.drawImage(farmers[99], xd, yd, null); 
					xd += 30;
				}
				else if(xd > 470) {
					yd += 30;
					xd = 200;

				}

				g.drawImage(farmers[i], xd, yd, null);
				xd += 30;
			}
		}
		xd = 600;
		yd = 500;
		// The resting room
		if(restingworkers[4] > 0){
			for(int i = 0; i < available.length; i++) {
				if(available.length > 100){
					g.setColor(Color.BLACK);		
					g.drawString(": " + available.length, 800, 800);
					g.drawImage(available[99], xd, yd, null); 
					xd += 30;
				}
				else if(xd > 870) {
					yd += 30;
					xd = 600;
				}

				g.drawImage(available[i], xd, yd, null); 
				xd += 30;
			}
		}
		// The isle
		xd = 200;
		yd = 400;
		int switchit = 1;
		if((healthcare[4] + farm[2] + kitchen [4]) > 0){
			for(int i = 0; i < resting.length; i++) {


				if(xd > 870 && yd == 400) {
					yd += 34;
					xd = 200;
					switchit = 1;
				}
				else if(xd > 870 && yd == 434){
					yd+= 34;
					xd = 200;
					switchit = 1;
				}
				else if(xd >870 && yd == 468){

					yd = 110;
					xd = 500;
					switchit = 2;
				}
				else if(switchit == 2 && yd >= 400 && yd <= 500){
					yd =500;
				}
				else if(switchit == 2 && yd >= 800 ){
					yd = 100;
					xd += 32;
				}

				switch(switchit){
				case 1:
					g.drawImage(resting[i], xd, yd, null); 
					xd += 30;

					break;
				case 2:
					g.drawImage(resting[i], xd, yd, null); 
					yd += 30;

					break;

				default:
					switchit = 0;
				}	
			}
		}



		//Frame
		g.setColor(Color.DARK_GRAY);		//frame
		g.drawRect(199, 99, 700, 700);

		//Texttitlar
		g.setColor(Color.RED);		//topleft
		g.drawString("Kitchen", 330, 90);

		g.setColor(Color.BLUE);		//topright
		g.drawString("Hospital", 730, 90);

		g.setColor(Color.BLACK);		//topright
		g.drawString("Sickbay", 730, 265);

		g.setColor(Color.YELLOW);		//bottomleft
		g.drawString("Farm", 330, 820);

		g.setColor(Color.GREEN);		//bottomright
		g.drawString("Homes", 730, 820);

		//TOPLEFT KITCHEN
		g.setColor(Color.BLACK);		//Statusvariables
		g.drawString("Rawmaterials: " + kitchen[1], 10, 75);

		g.setColor(Color.BLACK);		//Statusvariables
		g.drawString("Available food: " + kitchen[2], 10, 125);


		g.setColor(Color.BLACK);		//Statusvariables
		g.drawString("Spacestation total: " + (((healthcare[4] + farm[2] + kitchen [4])+(farm[1]+kitchen[3]+healthcare[3])+restingworkers[4]+healthcare[2])), 10, 175);

		g.setColor(Color.BLACK);		//Statusvariables
		g.drawString("Sick workers: " + healthcare[2], 10, 225);

		g.setColor(Color.BLACK);		//Statusvariables
		g.drawString("Available medicine: " + healthcare[1], 10, 275);



		repaint();
	}
}	

