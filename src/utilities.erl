%% @doc utilities module
-module(utilities).
-export([get_data/2, count_elements/1, count_workers/1]).
%% @doc Returns the second element of a tuple with the given Key in a tuplelist.
-spec get_data(Key, List) -> Data when
    Data::any(),
    Key::atom(),
    List::[{atom(), Data}].

get_data(Key, List) ->
    {_, Data} = lists:keyfind(Key, 1, List),
    Data.

%% @doc Returns the number of elements in list.
-spec count_elements(List) -> N when
    N::integer(),
    List::[any()].
count_elements([]) ->
    0;
count_elements([_ | Tail]) ->
    1+count_elements(Tail).

%% @doc Returns a tuple with the number of {Doctors, Farmers, Chefs} in a tuple list of workers.
-spec count_workers(List) -> Tuple when
    Tuple::{integer(), integer(), integer()},
    List::[{atom(), pid()}].
count_workers (List) ->
    count_workers(0, 0, 0, List).
count_workers(Doctors, Farmers, Chefs, []) ->
    {Doctors, Farmers, Chefs};
count_workers(Doctors, Farmers, Chefs, [{doctor, _} | List]) ->
    count_workers(Doctors+1, Farmers, Chefs, List);
count_workers(Doctors, Farmers, Chefs, [{farmer, _} | List]) ->
    count_workers(Doctors, Farmers+1, Chefs, List);  
count_workers(Doctors, Farmers, Chefs, [{chef, _} | List]) ->
    count_workers(Doctors, Farmers, Chefs+1, List).