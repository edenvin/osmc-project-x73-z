%% @doc Operations module
-module(operations).
-export([init/0]).
%% @doc Initializes the module which spawns new processes which enters the following loops:
%%      kitchen_loop which can receive the following messages:
%%      {update_resource, Resource_name, Amount, Answer_PID} : Updates the given resource Resource_name with the Amount and sends a message to Answer_PID if it was successful or not and updates the GUI if it was.
%%      {change_status, New_status, PID} : Updates the list of workers with the New_status and PID, updates the GUI.
%%      tick : Checks how much food there is and determines if a free chef should start working or not.
%%      {how_much_resource, Resource_name, PID} : Sends the amount of the resource Resource_name to PID.
%%      {workers, Sender} : Sends a list of the working and the free worker to the Sender.
%%
%%      hospital_loop which can receive the following messages:
%%      {update_resource, Resource_name, Amount, Answer_PID} : Same as in kitchen_loop.
%%      {change_status, New_status, PID} : Same as in kitchen_loop.
%%      tick : Checks how much medicine there is and determines if a free doctor should start working or not.
%%      {im_sick, PID} : Adds the PID to the list of sick workers and updates the GUI.
%%      {im_cured, PID} : Removes the PID from the list of sick workers and updates the GUI.
%%      {workers, Sender} : Same as in kitchen_loop.
%%      
%%      farm_loop which can receive the following messages:
%%      {change_status, New_status, PID} : Same as in kitchen_loop.
%%      tick : Cheks how much rawmaterial there is in the kitchen and determines if a free farmer should start working or not.
%%      {workers, Sender} : Same as in kitchen_loop.
%%
%%      resting_loop which can receive the following messages:
%%      {now_resting, Type, PID} : Adds the PID with the Type to the list of resting workers.
%%      {now_available, Type, PID} : Removes the PID with the Tupe from the list of resting workers.
%%
%%      The main module process waits for the following message:
%%      {'EXIT', _PID, terminate} : Exits normally.
-spec init() -> 
ok.

init() ->
    process_flag(trap_exit, true),
    register(kitchen, spawn_link(fun()->kitchen_loop([]) end)),
	register(hospital, spawn_link(fun()->hospital_loop([]) end)),
	register(farm, spawn_link(fun()->farm_loop([]) end)),
	register(restingroom, spawn_link(fun()->restingroom_loop([]) end)),
    workers ! ready,
    loop().

loop() ->
    receive
        {'EXIT', _PID, terminate} ->
            exit(normal)
    end.

kitchen_loop([]) ->
    kitchen_loop([  {rawmaterial, 0},
                    {food, 0},
                    {working_workers, []},
                    {available_workers, []}
    ]);
kitchen_loop(Info) ->
    receive
        {update_resource, Resource_name, Amount, Answer_PID} ->
            {Changed, New_info} = update_resource(Resource_name, Amount, Info, Answer_PID),
            if
                Changed ->
                    update_gui(kitchen, New_info);
                true ->
                    ok
            end,
            kitchen_loop(New_info);
        {change_status, New_status, PID} ->
            New_info = change_status(New_status, PID, Info),
            update_gui(kitchen, New_info),
            kitchen_loop(New_info);
        tick ->
            Food = utilities:get_data(food, Info),
            workers ! {nr_workers, self()},
            receive
                {nr_workers, Nr_workers} ->
                    _Limit = Nr_workers div 3,
                    if
                        _Limit < 11 ->   
                            Limit = 10;
                        true ->
                            Limit = _Limit
                    end
            end,
            if
                Food < Limit ->
                    Available_workers = utilities:get_data(available_workers, Info),
                    if
                        Available_workers =/= [] ->
                            [Worker | _] = Available_workers,
                            Worker ! work;
                        true ->
                            ok
                    end;
                true ->
                    ok
            end,
            kitchen_loop(Info);
        {how_much_resource, Resource_name, PID} ->
        	PID ! {this_much_resource, utilities:get_data(Resource_name, Info)},
            kitchen_loop(Info);
        {workers, Sender} ->
            Sender ! {workers, utilities:get_data(working_workers, Info), utilities:get_data(available_workers, Info)},
            kitchen_loop(Info)
    end.

hospital_loop([]) ->
	hospital_loop([ 	{medication, 0},
						{sick_workers, []},
					 	{working_workers, []},
					 	{available_workers, []}
		]);

hospital_loop(Info) ->
	receive
		{update_resource, Resource_name, Amount, Answer_PID} ->
            {Changed, New_info} = update_resource(Resource_name, Amount, Info, Answer_PID),
            if
                Changed ->
                    update_gui(hospital, New_info);
                true ->
                    ok
            end,
            hospital_loop(New_info);
        {change_status, New_status, PID} ->
			New_info = change_status(New_status, PID, Info),
			update_gui(hospital, New_info),
            hospital_loop(New_info);
        tick ->
            Medication = utilities:get_data(medication, Info),
            workers ! {nr_workers, self()},
            receive
                {nr_workers, Nr_workers} ->
                    _Limit = Nr_workers div 3,
                    if
                        _Limit < 11 ->   
                            Limit = 10;
                        true ->
                            Limit = _Limit
                    end
            end,
            if
                Medication < Limit ->
                    Available_workers = utilities:get_data(available_workers, Info),
                    if
                        Available_workers =/= [] ->
                            [Worker | _] = Available_workers,
                            Worker ! work;
                        true ->
                            ok
                    end;
                true ->
                    ok
            end,
            hospital_loop(Info);
        {im_sick, PID} ->
        	New_sick_list = [PID | utilities:get_data(sick_workers, Info)],
			New_info = lists:keyreplace(sick_workers, 1, Info, {sick_workers, New_sick_list}),
			update_gui(hospital, New_info),
            hospital_loop(New_info);
        {im_cured, PID} ->
        	New_sick_list = lists:delete(PID, utilities:get_data(sick_workers, Info)),
    		New_info = lists:keyreplace(sick_workers, 1, Info, {sick_workers, New_sick_list}),
    		update_gui(hospital, New_info),
    		hospital_loop(New_info);
        {workers, Sender} ->
            Sender ! {workers, utilities:get_data(working_workers, Info), utilities:get_data(available_workers, Info)},
            hospital_loop(Info)
    end.

farm_loop([]) ->
	farm_loop([		{working_workers, []},
					{available_workers, []}

		]);

farm_loop(Info) ->
	receive
		{change_status, New_status, PID} ->
			New_info = change_status(New_status, PID, Info),
			update_gui(farm, New_info),
            farm_loop(New_info);
		tick ->
            kitchen ! {how_much_resource, rawmaterial, self()},
            workers ! {nr_workers, self()},
            receive 
            	{this_much_resource, Amount_resource} ->
                receive
                    {nr_workers, Nr_workers} ->
                        _Limit = Nr_workers div 3,
                    if
                        _Limit < 11 ->   
                            Limit = 10;
                        true ->
                            Limit = _Limit
                    end
                end,
           			if
            	 		Amount_resource < Limit ->
                     	   	Available_workers = utilities:get_data(available_workers, Info),
                       		if
                           		Available_workers =/= [] ->
                               		[Worker | _] = Available_workers,
                                	Worker ! work;
                            	true ->
                            		ok
                        	end;
                        true ->
                    	   ok
            		end
            end,
       		farm_loop(Info);
        {workers, Sender} ->
            Sender ! {workers, utilities:get_data(working_workers, Info), utilities:get_data(available_workers, Info)},
            farm_loop(Info)
    end.

restingroom_loop([]) ->
	restingroom_loop([{resting_workers, []}
		]);

restingroom_loop(Info)	-> 
	receive
		{now_resting, Type, PID} ->
			New_resting_workers = [{Type, PID} | utilities:get_data(resting_workers, Info)],
			Nr_of_workers = utilities:count_workers(New_resting_workers),
			New_info = lists:keyreplace(resting_workers, 1, Info, {resting_workers, New_resting_workers}),
			update_gui(restingroom, Nr_of_workers),
        	restingroom_loop(New_info);
		{now_available, Type, PID} ->
			New_resting_workers = lists:delete({Type, PID}, utilities:get_data(resting_workers, Info)),
			Nr_of_workers = utilities:count_workers(New_resting_workers),
			New_info = lists:keyreplace(resting_workers, 1, Info, {resting_workers, New_resting_workers}),
			update_gui(restingroom, Nr_of_workers),
			restingroom_loop(New_info)
    end.		





change_status(New_status, PID, Info) ->
    New_working_list = lists:delete(PID, utilities:get_data(working_workers, Info)),
    New_avail_list = lists:delete(PID, utilities:get_data(available_workers, Info)),
    if
        New_status == free ->
            New_avail_list2 = [PID | New_avail_list],
            New_working_list2 = New_working_list;
        New_status == working ->
            New_working_list2 = [PID | New_working_list],
            New_avail_list2 = New_avail_list;
        New_status == sick ->
        	New_avail_list2 = New_avail_list,
        	New_working_list2 = New_working_list;
        true ->
            New_avail_list2 = New_avail_list,
            New_working_list2 = New_working_list
    end,
    Info2 = lists:keyreplace(working_workers, 1, Info, {working_workers, New_working_list2}),
    lists:keyreplace(available_workers, 1, Info2, {available_workers, New_avail_list2}).



update_resource(Resource_name, Amount, List, Answer_PID) ->
    New_data = Amount + utilities:get_data(Resource_name, List),
    if 
        New_data < 0 ->
            Answer_PID ! {error_resource, not_enough, Resource_name},
            {false, List};
        true ->
            Answer_PID ! {ok_resource, Resource_name},
            {true, lists:keyreplace(Resource_name, 1, List, {Resource_name, New_data})}
    end.

update_gui(kitchen, Info) ->
    {gui, 'java@localhost'} ! {kitchen, 
                utilities:get_data(rawmaterial, Info),
                utilities:get_data(food, Info),
                utilities:count_elements(utilities:get_data(working_workers, Info)),
                utilities:count_elements(utilities:get_data(available_workers, Info))};
update_gui(hospital, Info) ->
    {gui, 'java@localhost'} ! {hospital,
                utilities:get_data(medication, Info),
                utilities:count_elements(utilities:get_data(sick_workers, Info)),
                utilities:count_elements(utilities:get_data(working_workers, Info)),
                utilities:count_elements(utilities:get_data(available_workers, Info))};

update_gui(farm, Info) ->
    {gui, 'java@localhost'} ! {farm,
                utilities:count_elements(utilities:get_data(working_workers, Info)),
                utilities:count_elements(utilities:get_data(available_workers, Info))};

update_gui(restingroom, {Doctors, Farmers, Chefs}) ->
    {gui, 'java@localhost'} ! {restingroom, Doctors, Farmers, Chefs}.
                

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                                                          %%
%%             EUnit Test Cases                                             %%
%%                                                                          %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
