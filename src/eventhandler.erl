%% @doc Eventhandler module
-module(eventhandler).
-export([init/0]).

%% @doc Initializes the module which enters a loop to read the following lines from the terminal:
%%      slow. : Makes the java program update the graphics at every clock signal.
%%      fast. : Makes the java program update the graphics every time something changes in the simulation.
%%      step. : Steps the whole simulation one clock signal.
%%      play. : Starts an automatic stepper which sends clock signals every 500 ms.
%%      {play, Time}. : Starts an automatic stepper which sends clock signals every given Time in ms.
%%      pause. : Stops the automatic stepper.
%%      {kill, Type}. : Terminates a worker with the given Type.
%%      {sick, Type, Time} : Makes a worker with the given Type sick for Time clocksignals.
%%      {hire, Type} : Spawns a new worker with the given Type.
%%      {loop, Count, Command} : Loops the given Command Count times.
-spec init() -> ok.

init() ->
    loop().

loop() ->
    Read = io:read(">> "),
    {Status, Term} = Read,
    if
        Status =:= error ->
            io:format("Try again1\n");
        true ->
            handle_command(Term)
    end,
    loop().

handle_command(Term) ->
    if
        is_tuple(Term) ->
            handle_tuples(Term);

        Term =:= slow ->
            {gui, 'java@localhost'} ! {slow};

        Term =:= fast ->
            {gui, 'java@localhost'} ! {fast};

        Term =:= step ->
            clock ! step;

        Term =:= play ->
            clock ! {play, 500};

        Term =:= pause ->
            clock ! pause;

        Term =:= exit ->
            x73z ! exit;

        true ->
            io:format("Try again\n")
    end.

handle_tuples({play, Speed}) ->
    clock ! {play, Speed};

handle_tuples({kill, Victim}) ->
    workers ! {remove, Victim};

handle_tuples({sick, Victim, Time}) ->
    workers ! {sick, Victim, Time};

handle_tuples({hire, Worker}) ->
    workers ! {add, Worker};

handle_tuples({loop, Count, Command}) ->
    looper(Count, Command);

handle_tuples({_, _}) ->
    io:format("Try again\n").

looper(1, Command) ->
    handle_command(Command);
looper(Count, Command) ->
    handle_command(Command),
    looper(Count-1, Command).
