%% @doc X73-Z Project.
-module(x73z).
-export([start/1]).

%% @doc Starts the simulation with N workers. 
%%      Spawns new processes which initializes the modules needed for the simulation to work.
%%      See the specific modules for more information.
-spec start(N) -> ok when
    N::integer() | string().

start(N) ->
    if
        is_integer(N) ->
            Number = N;
        true ->
            [_N | _] = N,
            {Number, _} = string:to_integer(_N)
    end,
    process_flag(trap_exit, true),
    register(x73z, self()),
    register(workers, spawn_link(fun()->workers:init(Number) end)),
    register(clock, spawn_link(fun()->clock:init() end)),
    register(eventhandler, spawn_link(fun()->eventhandler:init() end)),
    register(operations, spawn_link(fun()->operations:init() end)),
    register(eventgenerator, spawn_link(fun()->eventgenerator:init() end)),
    loop().

loop() ->
    receive
        exit ->
            exit(terminate);
        {'EXIT', _PID, normal} ->
            loop();
        {'EXIT', _PID, Reason} ->
            io:fwrite(Reason),
            Info = process_info(_PID, registered_name),
            io:write(Info),
            io:fwrite("\n"),
            loop()
    end.
