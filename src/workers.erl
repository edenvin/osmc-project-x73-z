%% @doc Workers module
-module(workers).
-export([init/1]).
-include_lib("eunit/include/eunit.hrl").

%% @doc Initializes the module with N workers where 1/3 is chefs, 1/3 is doctor and 1/3 is farmers. 
%%      This process goes into a loop which will have a tuple list of all the workers and wait for the following messages:
%%      {add, Type} : Spawns a new worker with the given type and adds it to the list of workers.
%%      {remove, Worker} : Kills the given worker and removes it from the list of workers.
%%      {find_workers, Type, Sender} : Sends a list of workers with the given type to the sender.
%%      {all_workers, Sender} : Sends a list of all the workers to the sender.
%%      {nr_workers, Sender} : Sends the number of workers to the sender.
%%      {sick, Worker, Time} : Spawns a new process which makes a given worker sick.
%%      {'EXIT', _PID, terminate} : Exits normally.
%%
%%      A worker is a process which will wait for the following messages:
%%      {state, Sender}: Sends the state of the worker to the sender.
%%      rest : Changes the state of the worker to rest.
%%      tick : Does different things depending of what state the worker is in.
%%      work : Changes the state of the worker to working.
%%      {get_sick, Time} : Makes the worker sick with the given time.
%%      seppuku : The worker terminates.
%%
%%

-spec init(N) -> ok when
    N::integer().

init(N) ->
    process_flag(trap_exit, true),
    Types = [chef, doctor, farmer],
    Nr_of_workers = count_elements(Types),
    Q = N div Nr_of_workers,
    R = N rem Nr_of_workers,
    receive
        ready ->
            ok
    end,
    Workers = start_workers(Types, Nr_of_workers-R, Q, []),
    super_loop({Workers, N}).

start_workers([], _, _, Acc) ->
    Acc;

start_workers([Type | Types], 0, Q, Acc) ->
    start_workers(Types, 0, Q, [{Type, add([], Q+1, Type)} | Acc]);

start_workers([Type | Types], N, Q, Acc) ->
    start_workers(Types, N-1, Q, [{Type, add([], Q, Type)} | Acc]).

add(List, Type) ->
    Worker = spawn_link(fun()->worker_init({free, 8, 0, 0, random:uniform(8), Type}) end),
    [Worker | List].

add(List, 0, _) ->
    List;

add(List, N, Type) ->
    add(add(List, Type), N-1, Type).

remove(List, Key) when is_atom(Key) ->
    Worker_list = find_workers(List, Key),
    if
        Worker_list =:= [] ->
            {List, false};
        true ->
            [Victim | Key_list] = Worker_list,
            Victim ! seppuku,
            {lists:keyreplace(Key, 1, List, {Key, Key_list}), true}
    end.

sick(chef, Time) ->
    kitchen ! {workers, self()},
    sick_receive(Time);

sick(doctor, Time) ->
    hospital ! {workers, self()},
    sick_receive(Time);

sick(farmer, Time) ->
    farm ! {workers, self()},
    sick_receive(Time);

sick(Key, Time) when is_pid(Key) ->
    Key ! {get_sick, Time}.

sick_receive(Time) ->
    receive
        {workers, Working_workers, Available_workers} ->
            if
                Working_workers =/= [] ->
                    hd(Working_workers) ! {get_sick, Time};
                Available_workers =/= [] ->
                    hd(Available_workers) ! {get_sick, Time};
                true ->
                    ok
            end
    end.

find_workers(List, Type) ->
    {_, PID_list} = lists:keyfind(Type, 1, List),
    PID_list.

find_workers(List, Type, State) ->
    {_, PID_list} = lists:keyfind(Type, 1, List),
    [PID ! {state, self()} || PID <- PID_list],
    [PID || PID <- PID_list, receive {state, Received_state} -> Received_state =:= State end].

super_loop(Info) ->
    {List, N} = Info,
    receive
        {add, Type} ->
            L = find_workers(List, Type),
            New_list = lists:keyreplace(Type, 1, List, {Type, add(L, Type)}),
            super_loop({New_list, N+1});
        {remove, Worker} ->
            {New_list, Removed} = remove(List, Worker),
            if
                Removed ->
                    super_loop({New_list, N-1});
                true ->
                    super_loop({New_list, N})
            end;
        {find_workers, Type, Sender} ->
            Sender ! {find_workers, find_workers(List, Type)},
            super_loop(Info);
        {all_workers, Sender} ->
            Sender ! {all_workers, List},
            super_loop(Info);
        {nr_workers, Sender} ->
            Sender ! {nr_workers, N},
            super_loop(Info);
        {sick, Worker, Time} ->
            spawn_link(fun()->sick(Worker, Time) end),
            super_loop(Info);
        print ->
            io:fwrite("~w\n", [Info]),
            super_loop(Info);
        {'EXIT', _PID, terminate} ->
            exit(normal);
        {'EXIT', _PID, _} ->
            super_loop(Info)
    end.

worker_init(Info) ->
    {_, _, _, _, _, Type} = Info,
    notify_operation(free, Type),
    worker_loop(Info).

worker_loop(Info) ->    
    {State, Working_time, Resting_time, Sick_time, Food, Type} = Info,
    receive
        {state, Sender} ->
            Sender ! {state, State},
            worker_loop(Info);
        rest ->
            worker_loop({rest, Working_time, Resting_time, Sick_time, Food, Type});
        tick when State =:= working ->
            works(Type),
            New_working_time = Working_time-1,
            New_resting_time = Resting_time+2,
            New_food = eat(Food),
            if 
                New_food =:= false ->
                    worker_loop (Info);
                true ->    
                    if
                        New_working_time =:= 0 ->
                            notify_operation(resting, Type),
                            restingroom ! {now_resting, Type, self()},
                            worker_loop({resting, New_working_time, New_resting_time, Sick_time, New_food, Type});
                        true ->
                            worker_loop({State, New_working_time, New_resting_time, Sick_time, New_food, Type})
                    end
            end;
        tick when State =:= resting ->
            New_resting_time = Resting_time-1,
            New_food = eat(Food),
            if 
                New_food =:= false ->
                    worker_loop (Info);
                true ->    
                    if
                        New_resting_time =:= 0 ->
                            restingroom ! {now_available, Type, self()},
                            notify_operation(free, Type),   
                            worker_loop({free, 8, 0, Sick_time, New_food, Type});
                        true ->
                            worker_loop({State, Working_time, New_resting_time, Sick_time, New_food, Type})
                    end
            end;
        tick when State =:= sick ->
            New_food = eat(Food),
            if 
                New_food =:= false ->
                    worker_loop(Info);
                true -> 
                    New_sicktime = get_medication(Sick_time),
                    if 
                        New_sicktime =:= false ->
                            worker_loop({State, Working_time, Resting_time, Sick_time, New_food, Type});
                        true ->    
                            if
                                New_sicktime =:= 0 ->
                                    hospital ! {im_cured, self()},
                                    notify_operation(free, Type),
                                    worker_loop({free, 8, 0, New_sicktime, New_food, Type});
                                true ->
                                    worker_loop({State, 0, 0, New_sicktime, New_food, Type})
                            end
                    end    
            end;
        tick when State =:= free ->
            New_food = eat(Food),
            if 
                New_food =:= false ->
                    worker_loop (Info);
                true ->  
                    worker_loop({State, Working_time, Resting_time, Sick_time, New_food, Type})
            end;
        work ->
            notify_operation(working, Type),
            worker_loop({working, Working_time, Resting_time, Sick_time, Food, Type});
        {get_sick, Time} ->
            if
                State =:= resting ->
                    restingroom ! {now_available, Type, self()};
                true ->
                    ok
            end,
            notify_operation(sick, Type),
            hospital ! {im_sick, self()},
            worker_loop({sick, Working_time, Resting_time, Time, Food, Type});  
        seppuku ->
            notify_operation(dying, Type),
            hospital ! {im_cured, self()},
            restingroom ! {now_available, Type, self()},
            exit(seppuku)
    end.

works(chef) ->
    kitchen ! {update_resource, rawmaterial, -1, self()},
        receive
            {error_resource, not_enough, rawmaterial} ->
                ok;
            {ok_resource, rawmaterial} ->
                kitchen ! {update_resource, food, 1, self()}
        end;

works(doctor) ->
    hospital ! {update_resource, medication, 1, self()},
        receive
            _ -> 
            ok
        end;   

works(farmer) ->
    kitchen ! {update_resource, rawmaterial, 1, self()},
        receive
            _ -> 
            ok
        end.   

eat(Food) ->
    if 
        Food =:= 0 ->
            kitchen ! {update_resource, food, -1, self()},
                receive
                    {error_resource, not_enough, _} ->
                        false;
                    {ok_resource, _} ->
                        8
                end;
        true ->
            Food-1  
    end.

get_medication(Sick_time)   ->
    hospital ! {update_resource, medication, -1, self()},
        receive
            {error_resource, not_enough, _} ->
                false;
            {ok_resource, _} ->
                Sick_time-1
        end.
                
notify_operation(State, chef) ->
    kitchen ! {change_status, State, self()};

notify_operation(State, repairman) ->
    workshop ! {change_status, State, self()};

notify_operation(State, doctor) ->
    hospital ! {change_status, State, self()};

notify_operation(State, farmer) ->
    farm ! {change_status, State, self()}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                                                          %%
%%             EUnit Test Cases                                             %%
%%                                                                          %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
count_elements([]) ->
    0;

count_elements([_ | List]) ->
    1+count_elements(List).

init_test_() ->
    Answer = [{repairman, [1, 2, 3]}, {doctor, [4, 5, 6]}, {farmer, [7, 8]}, {chef, [9, 10]}],
    Answer_count = [count_elements(E) || {_, E} <- Answer],
    Init_PID = spawn(fun()->init(10) end),
    Init_PID ! {all_workers, self()},
    receive
        {all_workers, All_workers} ->
            All_workers
    end,
    Test_count = [count_elements(PID) || {_, PID} <- All_workers],
    ?_assertEqual(Answer_count, Test_count).

add_test_() ->
    PIDS = add([], 3),
    Answer = free,
    [PID ! {state, self()} || PID <- PIDS],
    [receive {state, State} -> ?_assertEqual(Answer, State) end || _ <- PIDS].

remove_test_() ->
    Init_PID = spawn(fun()->init(10) end),
    Init_PID ! {all_workers, self()},
    receive
        {all_workers, All_workers} ->
            All_workers
    end,
    {Test1, _} = remove(All_workers, farmer),
    _ = ?_assertEqual(2, count_elements(find_workers(Test1, farmer))),
    [A_chef | _] = find_workers(Test1, chef),
    {Test2, _} = remove(Test1, A_chef),
    _ = ?_assertEqual(8, lists:foldl(fun(X, Acc)-> X + Acc end, 0, [count_elements(E) || {_, E} <- Test2])).

find_workers_test_() ->
    Init_PID = spawn(fun()->init(10) end),
    Init_PID ! {all_workers, self()},
    receive
        {all_workers, All_workers} ->
            All_workers
    end,
    All_repairmen = find_workers(All_workers, repairman),
    _ = ?_assertEqual(3, count_elements(All_repairmen)),
    [Repairman1, Repairman2 | _] = All_repairmen,
    Repairman1 ! rest,
    All_resting_repairmen1 = find_workers(All_workers, repairman, rest),
    _ = ?_assertEqual(1, count_elements(All_resting_repairmen1)),
    Repairman2 ! rest,
    All_resting_repairmen2 = find_workers(All_workers, repairman, rest),
    ?_assertEqual(2, count_elements(All_resting_repairmen2)).
